FROM yuriyvlasov/openssh-server
ENV SOCKS5_PORT 4145
COPY config.yml /config.yml
RUN apk add --update --no-cache nginx curl && \
	mkdir -p /run/nginx /usr/share/nginx/html && \
	mv /etc/nginx/mime.types /etc/nginx/default-mime.types

COPY default.conf /etc/nginx/conf.d/default.conf
COPY mime.types /etc/nginx/mime.types
COPY entrypoint.sh /bootstrap/10_entrypoint.sh

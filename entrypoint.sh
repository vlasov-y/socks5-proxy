#!/usr/bin/env sh
set -eo pipefail
cd /usr/share/nginx/html

if [ "$EXTERNAL_SSH_PORT" != '' ]; then
	EXTERNAL_SSH_PORT="-p ${EXTERNAL_SSH_PORT}"
fi

cat <<EOF | tee connect.sh
#!/usr/bin/env sh
set -ex
ssh ${EXTERNAL_SSH_PORT} -fND $SOCKS5_PORT `ls /home | head -1`@${SOCKS5_HOST:-`curl -fsSL4 ifconfig.co`}
EOF

cat <<EOF | tee proxy.pac
function FindProxyForURL(url, host) {
    socks5 = "SOCKS5 127.0.0.1:${SOCKS5_PORT};";
    proxied = [`echo "$PROXY_SITES" | sed -r 's/([^,]+)/"\1"/g'`];
    for (var i = proxied.length - 1; i >= 0; i--) {
        if (shExpMatch(host, proxied[i]) || shExpMatch(host, "*" + proxied[i]))
            return socks5;
    }
    // else
    return "DIRECT";
}
EOF
echo

nginx -g 'daemon off;'

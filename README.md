## SOCKS5 proxy  
Based on [openssh-server](https://hub.docker.com/r/yuriyvlasov/openssh-server). Expose SSH on 22 port and Nginx on 80. Nginx returns PAC file.  
  
**Pros:**  
* read one from base openssh image :)  
* provides proxy autoconfiguration file  
  
## Settings  
Default user and password are the same one: _tunnel_. You **should** change defaults using configuration which is described [here](https://hub.docker.com/r/yuriyvlasov/openssh-server).  

| Name | Default | Description |  
| ---- | ------- | ----------- |  
| SOCKS5_HOST | Public IPv4 | External domain name where this server will be reachable |  
| SOCKS5_PORT | 4145 | Port which will be used for SOCKS5 tunnel on target host |  
| EXTERNAL_SSH_PORT | 22 | **External** port which is forwarded to 22 port inside of container |  
| PROXY_SITES | '' | Comma-separated list of DNS wildcards |  

Container will run its SSH server on port 22 and web server on port 80, but we need to know exactly what port will be used for tunnel to this server in order to write it to PAC file. Just follow example below.  
  
### Example  
```yaml  
version: '3.5'  
services:  
  socks5:  
    image: yuriyvlasov/socks5-proxy  
    restart: unless-stopped  
    ports:  
      - "2020:22"  
      - "80:80"  
    environment:  
      - PROXY_SITES=yandex.*,mail.ru,vk.com,ok.ru,kinopoisk.ru,yastatic.*  
      - SOCKS5_PORT=1329  
      - EXTERNAL_SSH_PORT=2020  
    logging:  
      driver: json-file  
      options:  
        max-size: 10m  
```  
Connection script can be now obtained at path `/connect` and PAC file at `/pac`.  
In order to connect to the server do:  
```bash  
curl -fsSL localhost | sh -  
```  
  